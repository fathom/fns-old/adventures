<!-- This is for proposing a new Adventure (aka: Learning Adventure) in FNS --> 
<!-- Read more about them here: https://gitlab.com/fathom/fns/adventures -->

--------------------------------------------------------------------------------

## Content:
<!-- Describe what you'll be learning about within the world of Decentralized Systems -->


## Questions:
<!-- List the questions that will drive your inquiry (and that you aim to answer by running the Adventure) -->


## Process and Structure(s):
<!-- How will you go about the Adventure? -->
<!-- What learning Structure(s) will you be experimenting with? Add any available links -->
<!-- If you're pioneering a Structure, consider opening a Proposal issue for it in the Structures repo, and also linking to that -->


## Participants:
<!-- Who will be involved? -->


## Period:
<!-- What are the proposed Start and End dates for the Adventure? -->


## Location:
<!-- Where is the online location that you'll be running the Adventure? Link to it below -->


## Log:
<!-- Where and how will you be documenting your Adventure? -->
<!-- Will you be Reflecting on your Structure(s) in the Log, or separately?  -->
<!-- Add links below -->


## Artifacts:
<!-- What are the artifacts or outputs you anticipate from your Adventure? -->
<!-- (e.g., writings, drawings, source code, photos, video) -->
<!-- Add links when available -->
 

--------------------------------------------------------------------------------

**Don't Forget**:         
- add yourself below as an `Assignee`
- select a `Due date`

**Keep all links here updated throughout your Adventure!**

--------------------------------------------------------------------------------
<!-- Please don't delete!  -->
/label ~"Adventure"


