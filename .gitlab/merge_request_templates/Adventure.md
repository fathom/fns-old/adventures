FNS `Adventure` Merge Request (MR)
--------------------------------------------------------------------------------

## What does this MR add?
<!-- Briefly describe what this MR is about -->



## Related Issues
<!-- Mention the issue(s) this MR closes (with "Resolves #") or is related to (with "Relates to #") -->



## FNS Adventure Checklist

- [ ] **Content**: topic within Decentralized Systems
- [ ] **Questions**: questions asked and answered/revisited during Adventure
- [ ] **Process**: Learning Structures experimented with
- [ ] **Location**: link to online location for running of Adventure 
- [ ] **Log**: including reflections on process and Structures; plus summative Reflection
- [ ] **Artifacts** (optional): links to any outputs from learning 



--------------------------------------------------------------------------------
<!-- Please don't delete!  -->
/label ~"Adventure"
