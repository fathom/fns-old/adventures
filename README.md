# FNS Adventures

This is a repo for **Adventures**  run in the FNS community. 


## Description

**FNS Adventures** are based on [Learning Adventures](https://gitlab.com/fathom/fns/structures/tree/master/1-learning-adventures), which offer a standardized format for people to engage in formal learning outside of an institutional context. 

FNS Adventures support experiments in **Self-Directed Learning** that also integrate **Decentralized Systems, Learning Structures, and Code.**  

Each Adventure consists of three parts: a Map, a Log, and Artifacts.

## Process

### 1. The Map

The first step of an Adventure is creating the Map, a document that lays out the learning plan. For an FNS Adventure, you'll begin by opening an ~Adventure issue in this repo. The issue will facilitate the Map-making process.

The map should include the following:   
- **content:** _what_ you, the learner, will be exploring.  
**Content for an FNS Adventure should be scoped within the world of Decentralized Systems.**
- **questions:** the questions that will drive your inquiry 
- **process:** _how_ you will go about the Adventure    
  - **Note which [Structure(s)](https://gitlab.com/fathom/fns/structures) you'll be experimenting with.** 
  - Link to any issues for the Structure you're experimenting with; and if it's something you're pioneering, consider opening a Proposal issue for it in the Structures repo
- **participants:** _who_ will be involved
- **period:** proposed start and end dates for running the Adventure

The Map should also link to three things that will be key to carrying out the Adventure: the **Location**, the **Log**, and **Artifacts**
(note: these sections are all demarcated in the ~Adventure issue) 

#### The Location
The map should designate an online location where the Adventure is being run. 
This could be a personal website, a community platform like [are.na](https://are.na), a code repository such as [gitlab](https://gitlab.com/), or even a place like a [youtube](https://youtube.com) channel.

### 2. The Log
The Log is the learner's account of their Adventure. Whereas the Map lays out the plan in advance, the Log describes how the Adventure actually unfolds. It could be kept as a journal or blog, even embedded in the Location selected for the Adventure. It needn't be super formal or lengthy, but it should try to capture key moments on the journey. 

Return to the questions from your Map: use them for guidance, and record your answers to them in the Log. 

**Reflections**: the Log is where you can **reflect on any Structures you're using**; how you've implemented them and iterated on them. Link to any related posts you've written to issues in the Structures repo. Finally, at the conclusion of your Adventure write a reflection on it: how it went, which practices and Structures were and weren't effective, and thoughts on how they could be improved upon.


### 3. Artifacts
A learning Adventure should produce some Artifacts, or outputs, along the way. They could take any of a number of forms, such as source code, photos, drawings, or video. They might even consist of writings that are incorporated into the Log. As outputs of the learning process, artifacts needn't be pretty or polished. They can be reflective of an interim state. However, it's helpful if they are given some context. They should be assembled as files or links, and published along with the Map and Log. 


## Following FNS Adventures

All FNS Adventures can followed via this repo's [Issue Board](https://gitlab.com/fathom/fns/adventures/boards).

<!-- Add in more details about Issue Board (?) -->

Your ~Adventure issue will often be the gateway for others in the community to connect with what you're doing. Therefore, it's really important to keep all the links in your issue up to date, for the duration of your Adventure -- even if you're running your Adventure in a separate location.



